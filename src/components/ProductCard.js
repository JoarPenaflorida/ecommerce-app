import { useState, useEffect } from "react";
import { Card, Button, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {
  const { _id, name, stocks, price } = productProp;
  return (
    <Col key={_id}>
      <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>

          <Card.Subtitle>Price</Card.Subtitle>
          <Card.Text>₱{price}</Card.Text>
          <Card.Subtitle>Stocks</Card.Subtitle>
          <Card.Text>{stocks} available</Card.Text>
          <></>
          <Button as={Link} to={`/products/${_id}`} variant="primary">
            Add To Cart
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}
