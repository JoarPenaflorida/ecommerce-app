import { useState, useEffect } from "react";
import { Card, Col, Row, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function CartCard({ cartProp }) {
  const { productId, name, quantity, subTotal } = cartProp;
  const navigate = useNavigate();
  const removeToCart = (productId, name) => {
    console.log(name);
    console.log(productId);
    fetch(`${process.env.REACT_APP_API_URL}/carts/${productId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: `Successfully Removed ${name}`,
            icon: "success",
            text: "You have successfully removed from this course.",
          });
          navigate("/cart");
        } else {
          Swal.fire({
            title: "Something went wrong.",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  return (
    <Row>
      <Col md={12} className="my-3">
        <Card>
          <Card.Body>
            <Card.Title className="mb-3">{name}</Card.Title>
            <Card.Subtitle className="mb-2">Quantity</Card.Subtitle>
            <Card.Text>{quantity}</Card.Text>
            <Card.Subtitle className="mb-2 ">Price</Card.Subtitle>
            <Card.Text>₱ {subTotal}</Card.Text>

            <Button
              as={Link}
              to={`/carts/${productId}`}
              size="sm"
              variant="primary"
            >
              Update
            </Button>
            <Button
              className="mx-2"
              size="sm"
              variant="danger"
              onClick={() => removeToCart(productId, name)}
            >
              Remove
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
