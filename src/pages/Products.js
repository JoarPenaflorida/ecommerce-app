import { useEffect, useState, useContext } from "react";
import { Row, Col } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";

export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return user.isAdmin ? (
    <Navigate to="/admin" />
  ) : (
    <>
      <h1 className="text-center my-5">Products</h1>
      <Row md={2} xs={1} lg={4} className="g-3">
        {products}
      </Row>
    </>
  );
}
