import { Button, Table, Modal, Form, Badge } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function AdminDashboard() {
  const { user } = useContext(UserContext);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [showEdit, setShowEdit] = useState(false);
  const handleCloseEdit = () => setShowEdit(false);
  const handleShowEdit = () => setShowEdit(true);
  const [productId, setProductId] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [stocks, setStocks] = useState(0);
  const [cost, setCost] = useState(0);
  const [price, setPrice] = useState(0);
  const [allProducts, setAllProducts] = useState([]);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      (name !== "" && description !== "",
      stocks !== "",
      cost !== "",
      price !== "" && price > 0 && cost > 0 && price >= cost)
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, stocks, cost, price]);

  const openEdit = (id) => {
    console.log(id);
    setProductId(id);

    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setStocks(data.stocks);
        setCost(data.cost);
      });
    handleShowEdit(true);
  };
  const closeEdit = () => {
    setName("");
    setDescription("");
    setStocks("");
    setPrice(0);
    setStocks(0);
    setCost(0);
    handleCloseEdit(false);
  };
  const closeAdd = () => {
    setName("");
    setDescription("");
    setStocks("");
    setPrice(0);
    setStocks(0);
    setCost(0);
    handleClose(false);
  };
  const editProduct = (e) => {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        stocks: stocks,
        cost: cost,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Product succesfully Updated",
            icon: "success",
            text: `${name} is now updated`,
          });

          // To automatically add the update in the page
          fetchData();
          // Automatically closed the form
          handleCloseEdit();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });

          handleCloseEdit();
        }
      });

    setName("");
    setDescription("");
    setStocks(0);
    setPrice(0);
    setCost(0);
    setShow(false);
  };

  function addProduct(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        stocks: stocks,
        cost: cost,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Product Added Succesful",
            icon: "success",
            text: `${name} is now added to products.`,
          });
          setName("");
          setDescription("");
          setStocks(0);
          setPrice(0);
          setCost(0);
          setShow(false);
          fetchData();
        } else {
          Swal.fire({
            title: "Something went wrong.",
            icon: "error",
            text: `Please try again later.`,
          });
        }
      });
  }

  const archive = (id, productName) => {
    console.log(id);
    console.log(productName);

    fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Archive Successful",
            icon: "success",
            text: `${productName} is now inactive.`,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Archive unsuccessful",
            icon: "error",
            text: `Oops, Something went wrong. Please try again later.`,
          });
        }
      });
  };
  const unarchive = (id, productName) => {
    console.log(id);
    console.log(productName);

    fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Unarchive Successful",
            icon: "success",
            text: `${productName} is now inactive.`,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Unarchive unsuccessful",
            icon: "error",
            text: `Oops, Something went wrong. Please try again later.`,
          });
        }
      });
  };

  const fetchOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/orders/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.orders);
      });
  };
  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setAllProducts(
          data.map((product) => {
            return (
              <tr key={product._id}>
                <td>{product._id}</td>
                <td>{product.name}</td>
                <td className="text-end">{product.stocks}</td>
                <td className="text-end">₱ {product.cost}</td>
                <td className="text-end">₱ {product.price}</td>
                <td className="text-center">
                  {product.isSale ? (
                    <Badge bg="info">On Sale</Badge>
                  ) : (
                    <Badge bg="warning">Not On Sale</Badge>
                  )}
                </td>
                <td className="text-center">
                  {product.isFeatured ? (
                    <Badge bg="info">Featured</Badge>
                  ) : (
                    <Badge bg="warning">Not Featured</Badge>
                  )}
                </td>
                <td className="text-center">
                  {product.isActive ? "Active" : "Inactive"}
                </td>
                <td className="text-center">
                  {product.isActive ? (
                    <Button
                      variant="danger"
                      size="sm"
                      onClick={() => archive(product._id, product.name)}
                    >
                      Archive
                    </Button>
                  ) : (
                    <>
                      <Button
                        variant="success"
                        size="sm"
                        onClick={() => unarchive(product._id, product.name)}
                      >
                        Unarchive
                      </Button>
                      <Button
                        variant="primary"
                        size="sm"
                        className="mx-1"
                        onClick={() => openEdit(product._id)}
                      >
                        Edit
                      </Button>
                    </>
                  )}
                </td>
              </tr>
            );
          })
        );
      });
  };
  useEffect(() => {
    fetchData();
  }, []);

  return user.isAdmin ? (
    <>
      <Modal show={show} onHide={() => closeAdd()}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e) => addProduct(e)}>
            <Form.Group className="mb-3" controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Enter Product Name"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder="Enter Description"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="stocks">
              <Form.Label>Stocks</Form.Label>
              <Form.Control
                type="number"
                value={stocks}
                onChange={(e) => setStocks(e.target.value)}
                placeholder="Enter Stocks"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="cost">
              <Form.Label>Cost</Form.Label>
              <Form.Control
                type="number"
                value={cost}
                onChange={(e) => setCost(e.target.value)}
                placeholder="Enter Cost"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                placeholder="Enter Price"
                required
              />
            </Form.Group>

            {isActive ? (
              <Button
                variant="primary"
                type="submit"
                className="my-3"
                id="submitBtn"
              >
                Add Product
              </Button>
            ) : (
              <Button
                variant="danger"
                disabled
                type="submit"
                className="my-3"
                id="submitBtn"
              >
                Add Product
              </Button>
            )}
          </Form>
        </Modal.Body>
      </Modal>
      <Modal show={showEdit} onHide={() => closeEdit()}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={(e) => editProduct(e)}>
            <Form.Group className="mb-3" controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Enter Product Name"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                placeholder="Enter Description"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="stocks">
              <Form.Label>Stocks</Form.Label>
              <Form.Control
                type="number"
                value={stocks}
                onChange={(e) => setStocks(e.target.value)}
                placeholder="Enter Stocks"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="cost">
              <Form.Label>Cost</Form.Label>
              <Form.Control
                type="number"
                value={cost}
                onChange={(e) => setCost(e.target.value)}
                placeholder="Enter Cost"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="price">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                placeholder="Enter Price"
                required
              />
            </Form.Group>

            {isActive ? (
              <Button
                variant="primary"
                type="submit"
                className="my-3"
                id="submitBtn"
              >
                Edit Product
              </Button>
            ) : (
              <Button
                variant="danger"
                disabled
                type="submit"
                className="my-3"
                id="submitBtn"
              >
                Edit Product
              </Button>
            )}
          </Form>
        </Modal.Body>
      </Modal>
      <div className="mt-5 mb-3 text-center">
        <h1 className="mb-3">Admin Dashboard</h1>
        <Button variant="primary" onClick={handleShow}>
          Add Product
        </Button>
        <Button variant="secondary" className="mx-2">
          Show Orders
        </Button>
      </div>
      <Table striped bordered hover size="sm" responsive="sm">
        <thead>
          <tr>
            <th>Product ID</th>
            <th>Product Name</th>
            <th className="text-end">Stocks</th>
            <th className="text-end">Cost</th>
            <th className="text-end">Price</th>
            <th className="text-center">Sale</th>
            <th className="text-center">Featured</th>
            <th className="text-center">Status</th>
            <th className="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
    </>
  ) : (
    <Navigate to="/admin" />
  );
}
