import { useEffect, useState, useContext } from "react";
// import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import { Button, Form } from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  console.log(`First Name: ${email}`);
  console.log(`First Name: ${password}`);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data.accessToken);
        if (data.accessToken !== undefined) {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: `Welcome to Joe's E Commerce!`,
          });
        } else {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.firstName,
        });
      });
  };
  return user.id !== null ? (
    <Navigate to="/" end />
  ) : (
    <>
      <h1 className="my-5">Login</h1>
      <Form onSubmit={(e) => loginUser(e)}>
        <Form.Group className="mb-3" controlId="firstName">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter Email Address"
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="lastName">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Enter Password"
            required
          />
        </Form.Group>

        {isActive ? (
          <Button
            variant="success"
            className="my-3"
            type="submit"
            id="submitBtn"
          >
            Submit
          </Button>
        ) : (
          <Button
            variant="danger"
            className="my-3"
            type="submit"
            id="submitBtn"
            disabled
          >
            Submit
          </Button>
        )}
      </Form>
    </>
  );
}
