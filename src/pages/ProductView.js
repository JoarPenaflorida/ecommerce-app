import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import Swal from "sweetalert2";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function ProductView() {
  const navigate = useNavigate();
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const { user } = useContext(UserContext);

  useEffect(() => {
    console.log(productId);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);
  const addToCart = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: 1,
        subTotal: price,
        name: name,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: "Successfully Added To Cart",
            icon: "success",
            text: `You have successfully ${name} added to Cart.`,
          });
          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong.",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };
  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>₱ {price}</Card.Text>
              {user.id !== null ? (
                <Button
                  variant="primary"
                  size="lg"
                  onClick={() => addToCart(productId)}
                >
                  Add To Cart
                </Button>
              ) : (
                <Button as={Link} to="/login" variant="success" size="lg">
                  Login to Buy
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
