import { Carousel } from "react-bootstrap";
import MyImage from "../img/image1.jpeg";
import MyImage1 from "../img/image2.jpeg";
import MyImage2 from "../img/image3.jpeg";
export default function Home() {
  return (
    <>
      <h1>Home</h1>
      <p>Welcome to Joe's Ecommerce</p>
      <Carousel slide={false}>
        <Carousel.Item>
          <img className="d-block w-800" src={MyImage} alt="" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={MyImage1} alt="Second slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={MyImage2} alt="Third slide" />
        </Carousel.Item>
      </Carousel>
    </>
  );
}
