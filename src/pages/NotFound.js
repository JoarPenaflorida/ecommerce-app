import Banner from "../components/Banner";
export default function NotFound() {
  const data = {
    title: "404 - Not Found",
    description: "The page you are looking for is not found",
    destination: "/",
    label: "Back home",
  };

  return (
    <>
      <Banner data={data} />
    </>
  );
}
